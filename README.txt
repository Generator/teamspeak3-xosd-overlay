Teamspeak overlay for Linux.
========

Teamspeak3 overlay for Linux, which uses lua and xosd to output.

--- Requires ---
Teamspeak3
xosd

Xosd may be installed by your the package manager, for example:
apt-get install xosd

--- Installation ---
1) Put "osd" folder into "tamspeak3/plugin/lua_plugin"
2) Activate lua_plugin in teamspeak Settings->Plugins.
3) Put off testmodule in settings of lua_plugin. It is not necessary.
4) You can configure font, color, position of overlay by editing init.lua in osd folder.

--- Changelog ---
Version 0.1
	First realize
